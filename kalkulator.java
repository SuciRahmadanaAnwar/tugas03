import java.util.InputMismatchException;
import java.util.Scanner;
 class kalkulator extends aplikasi_kalkulator {
	 static Scanner input=new Scanner(System.in); 
	 //membuat 3 buah variabel
    private double a;
    private double b;
    private double hasil;
  
    //membuat method set dan get
    public void seta(double n){ 
    	a = n; 
    	}
    
    public double geta(){ 
    	return a; 
    	}
    
    public void setb(double n){ 
    	b = n; 
    	}
    
    public double getb(){
    	return b; 
    	}
  
    
    public double gethasil(){
    	return  hasil; 
    	}
  
    //membuat langkah proses
    public void addition(){
        hasil = a+b;
    }
  
    public void subtraction(){
        hasil = a-b;
    }
    public void multiplication(){
    	hasil = a*b;
    }
    public void Division(){
    	hasil = a/b;
    }
    public void powers(){
    	 hasil = Math.pow(a, b);
    }
    public void akar (){
   	 hasil = Math.sqrt(a);
    }

	 
	public static void main(String[] args){
        //membuat objek baru
        kalkulator x = new kalkulator();
        Scanner input = new Scanner(System.in);
        double a,b ; 
        do{
        	String choice="l";
			int pilihan=-1 ;
       
            System.out.println();
            System.out.println("=========================");
            System.out.println("\t\tKalkulator");
            System.out.println("1. Penjumlahan");
            System.out.println("2. Pengurangan");
            System.out.println("3. Perkalian");
            System.out.println("4. Pembagian");
            System.out.println("5. pangkat");
            System.out.println("6. akar");
            System.out.println("0. keluar");
            System.out.println("=========================");
            System.out.print("Silahkan masukkan pilihan anda : ");
            System.out.println();
            try{
				pilihan = input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
				System.out.print("DAN ");
		}	    		
			input.nextLine();	
			
			switch(pilihan){
			case 0:
				break;
			case 1:
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				System.out.print("Masukkan Nilai b : ");
				b = input.nextDouble();
				 x.setb(b);
				x.addition();
	            System.out.println("Hasil Penjumlahan = "+x.gethasil());
				break;
			case 2:   			
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				System.out.print("Masukkan Nilai b : ");
				b = input.nextDouble();
				 x.setb(b);
				x.subtraction();
				System.out.println("Hasil Pengurangan = "+x.gethasil());
	            break;
			case 3:
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				System.out.print("Masukkan Nilai b : ");
				b = input.nextDouble();
				 x.setb(b);
				x.multiplication();
				System.out.println("Hasil Perkalian = "+x.gethasil());
				break;
			case 4:
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				System.out.print("Masukkan Nilai b : ");
				b = input.nextDouble();
				 x.setb(b);
				x.Division();
				System.out.println("Hasil Pembagian = "+x.gethasil());
				break;
			case 5:
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				System.out.print("Masukkan Nilai b : ");
				b = input.nextDouble();
				 x.setb(b);
				x.powers();
				System.out.println("Hasil Pangkat = "+x.gethasil());
				break;
			case 6:
				System.out.print("Masukkan Nilai a : ");
				a = input.nextDouble();
				x.seta(a);
				x.akar();
				System.out.println("Hasil Akar = "+x.gethasil());
				break;
			default:
				System.out.println("Tidak sesuai dengan pilihan yang ada :)");
				do{ 
				    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("=================================");
				    
					    if (choice.contentEquals("YA")){
					    	break;
					    }
					    else if(choice.contentEquals("TIDAK")){
					    	break;
					    }
					    else {
					    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
					    }
					    
		    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
				break;
			}
					if (pilihan == 0)break;
				    if (choice.contentEquals("TIDAK"))break;
				    if (choice.contentEquals("YA"))continue;
				
			}while(true);
		}
}