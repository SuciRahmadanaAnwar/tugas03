import java.util.InputMismatchException;
import java.util.Scanner;

class menu extends bangundatar{
		static Scanner input=new Scanner(System.in); 
		 //membuat 3 buah variabel
	    private float side,side1,side2,side3,side4,length,width,base,height,diagonal1,diagonal2,radius;
	    private double large, perimeter, Volume, surfArea, s, heightside, largepedestal1, largeside;
	    
	    
	public void setside(float n){
		side =  n;
	}
	public float getside (){
		return side;
	}
	public void setside1(float n){
		side1 = n;
	}
	public float getside1(){
		return side1;
	}
	public void setside2(float n){
		side2 =  n;
	}
	public float  getside2 (){
		return side2;
	}
	public void setside3(float n){
		side3 =  n;
	}
	public float  getside3 (){
		return side3;
	}
	public void setside4(float n){
		side4 =  n;
	}
	public float  getside4 (){
		return side4;
	}
	public void setlength(float n){
		length =  n;
	}
	public float  getlength (){
		return length;
	}
	public void setwidht(float n){
		width =  n;
	}
	public float  getwidht (){
		return width;
	}
	public void setheight(float n){
		height =  n;
	}
	public float  getheight (){
		return height;
	}
	public void setbase(float n){
		base =  n;
	}
	public float  getbase (){
		return base;
	}
	public void setdiagonal1(float n){
		diagonal1 =  n;
	}
	public float  getdiagonal1 (){
		return diagonal1;
	}
	public void setdiagonal2(float n){
		diagonal2 =  n;
	}
	public float  getdiagonal2 (){
		return diagonal2;
	}
	public void setradius(float n){
		radius =  n;
	}
	public float  getradius (){
		return radius;
	}
	public double getlarge (){
		return large;
	}
	public double getperimeter (){
		return perimeter;
	}
	public double getVolume(){
		return Volume;
	}
	public double getsurfArea(){
		return surfArea;
	}
	public double getheightside(){
		return heightside;
	}
	public double getlargeside(){
		return largeside;
	}
	public double getlargepedestal(){
		return largepedestal1;
	}
	
	//bangun datar
	public void square(){
		 large = side*side;
	 perimeter = 4*side;
	}
	public void rectangle(){
		 large = length*width;
		 perimeter = ((2*length)+(2*width));
		
	}
	public  void trapezoid(){
		 large = (0.5*(side1+side2)*height);
		 perimeter = side1*side2*side3*side4;
	}
	public  void IsoscelesTriangle(){
		 large = (0.5*(base*height));
		 perimeter = (base+(2*side));
	}
	public  void rhombus(){
		 large = (0.5*(diagonal1*diagonal2));
		 perimeter = 4*side;
	}
	public  void circle(){
		 large = (3.14*(radius*radius));
		 perimeter = (2*(3.14*radius));
		
	}
	public  void kyte(){
		 large = (0.5*(diagonal1*diagonal2));
		 perimeter = ((2*side1)+(2*side2));
		
	}
	public  void parallelogram(){
		 large = base*height;
		perimeter = ((2*base)+(2*side));
		
	}
	public  void EquilateralTriangle(){
		 large = (0.5*(side*height));
		 perimeter = 3*side;
	}
	//bangun ruang
	public  void beam (){
		 Volume = length*width*height;
		 surfArea = (2*(length*width)+(length*height)+(width*height));
	}
	public void cube (){
		 Volume = side*side*side;
		 surfArea = (6*(side*side));
	}
	public void tube(){
		Volume = (3.14*((radius*radius)*height));
	    surfArea = (2*3.14*(radius*(radius+height)));
	}
	public void cone(){
	 Volume = (((1/3)*3.14*((radius*radius)*height)));
	 surfArea = (3.14*radius*(radius+side));	
	}
	public void triangularprism(){
		perimeter = (2*(side1+side2+side3)+(3*height));
		s = 0.5*perimeter;
	 	large = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	 	surfArea = (side1+side2+side3)*height*2*large;
	}
	public void ball(){
		Volume = ((3.14*(radius*radius*radius))*4/3);
		surfArea = 4 * 3.14 * radius * radius;
	}
	public void rectangularpyramid(){
		double largepedestal = Math.pow(side,2);
		heightside = Math.sqrt(Math.pow(0.5*side,2)*Math.pow(height,2));
		largeside = 0.5*side*heightside;
		Volume =( 1/3*(largepedestal*height));
		surfArea = (largepedestal+(4*largeside));
	}

 public class bangundatar{
	public  void main(String[] args) {
		menu datar = new menu();
		Scanner input = new Scanner (System.in);
		String choice="x";
    	int pilihan=-1;
    	int pilihan1=-1;
    	int menu=-1;
    	float diagonal1=1, diagonal2=1, side=1, base=1, height=1, side1=1, side2=1, width=1, radius=1;
		do{
    		
        System.out.println("\n");
		System.out.println("---------Bangun Datar dan Bangun Ruang---------");
		System.out.println("***********************************************"); 		
		System.out.println("1. Bangun Datar");
		System.out.println("2. Bangun Ruang");
		System.out.println("0. Keluar");
		System.out.println("***********************************************");
			System.out.print("Silahkan masukkan nomor yang anda pilih : ");
			try{
    			menu = input.nextInt();	    			
    		}catch(InputMismatchException e){
    			System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
    			System.out.print("DAN ");
    		}	    		
	
		 input.nextLine();			
		switch(menu){
		case 0:
			break;
		case 1:
			
		System.out.println("\n");
		System.out.println("-----------------Bangun Datar------------------");
		System.out.println("***********************************************"); 	
		System.out.println("1. Persegi");
		System.out.println("2. Persegi Panjang");
		System.out.println("3. Trapesium");
		System.out.println("4. Segitiga sama kaki");
		System.out.println("5. Belah Ketupat");
		System.out.println("6. Lingkaran");
		System.out.println("7. Layang-layang");
		System.out.println("8. Jajargenjang");
		System.out.println("9. Segitiga sama sisi");
		System.out.println("0. Keluar");
		System.out.println("***********************************************"); 	
			System.out.print("Silahkan masukkan nomor yang anda pilih : ");
			try{
				pilihan = input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
				System.out.print("DAN ");    		
			input.nextLine();	
			do{ 
			    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("=================================");
			    
				    if (choice.contentEquals("YA")){
				    	break;
				    }
				    else if(choice.contentEquals("TIDAK")){
				    	break;
				    }
				    else {
				    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }
				    
	    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
				break;
			}
				if (pilihan == 0)break;
			    if (choice.contentEquals("TIDAK"))break;
			    if (choice.contentEquals("YA"))continue;
		
		switch(pilihan){
		case 0:
			break;
		
		case 1 :
			
			do{
				try{
					System.out.println();
					System.out.print("Masukkan panjang sisi : ");
					side = input.nextFloat();
					datar.setside(side);
					
					if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	datar.square();
					System.out.println("Luas dari persegi adalah "+datar.getlarge());
					System.out.println("Keliling dari persegi adalah "+datar.getperimeter());
			    	System.out.println();
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				    
			}while (true);	
			break;
		case 2 :
			float length;
			do{
				try{
					System.out.println("Masukkan panjang persegi panjang : ");
					length = input.nextFloat();
					datar.setlength(length);
			    	if (length < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (length == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	
			    	System.out.println("Masukkan lebar persegi panjang : ");
			    	width  = input.nextFloat();
			    	datar.setlength(width);
			    	
					if (width < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (width == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	datar.rectangle();
			    	System.out.println("Luas dari persegi panjang adalah "+datar.getlarge());
			    	System.out.println("Keliling dari persegi panjang adalah "+datar.getperimeter());
			    	System.out.println();
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;

			}while (true);	
			break;
		case 3:
			float side3, side4;
			
			do{
				try{
					System.out.println("Masukkan sisi atas trapesium : ");
					side1 = input.nextFloat();
					datar.setside1(side1);
					if (side1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side1 == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	
			    	System.out.println("Masukkan sisi bawah trapesium : ");
					side2 = input.nextFloat();
					datar.setside2(side2);
					if (side2 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side2 == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println("Masukkan sisi miring trapesium : ");
					side3 = input.nextFloat();
					datar.setside3(side3);
					
					if (side3 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side3 == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println("Masukkan sisi miring trapesium : ");
					side4 = input.nextFloat();
					datar.setside4(side4);
					if (side4 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side4 == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println("Masukkan tinggi trapesium : ");
					height = input.nextFloat();
					datar.setheight(height);
					
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}	
					datar.trapezoid();
					System.out.println("Luas dari trapesium adalah "+datar.getlarge());
					System.out.println("Keliling dari Trapesium adalah "+datar.getperimeter());
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;	
				
			}while (true);
			break;
		
		case 4:


			do{
				try{
					System.out.println("Masukkan panjang alas segitiga : ");
					base = input.nextFloat();
					datar.setbase(base);
					if (base < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (base == 0) {
			    	    throw new ArithmeticException();
			    	}
			    	System.out.println("Masukkan panjang sisi miring segitiga : ");
					side = input.nextFloat();
					datar.setside(side);
					
					if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan tinggi segitiga : ");
					height = input.nextFloat();
					datar.setheight(height);
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}
				
			    	datar.IsoscelesTriangle();
					System.out.println("Luas dari segitiga sama kaki adalah "+datar.getlarge());
					System.out.println("Keliling dari segitiga sama kaki adalah "+datar.getperimeter());
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);	
		case 5:
			do{
				try{
					System.out.println("Masukkan panjang diagonal 1 belah ketupat : ");
					diagonal1 = input.nextFloat();
					datar.setdiagonal1(diagonal1);
					if (diagonal1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (diagonal1 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan panjang diagonal 2 belah ketupat: ");
					diagonal2 = input.nextFloat();
					datar.setdiagonal2(diagonal2);
					
					if (diagonal2 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (diagonal2 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan panjang sisi belah ketupat : ");
					side = input.nextFloat();
					datar.setside(side);
					if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}
					datar.rhombus();
					System.out.println("Luas dari belah ketupat adalah "+datar.getlarge());
					System.out.println("Keliling dari belah ketupat adalah "+datar.getperimeter());
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);
		
		case 6:
			
			do{
				try{
					System.out.println("Masukkan jari-jari Lingkaran : ");
					radius = input.nextFloat();
					datar.setradius(radius);
					if (radius < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (radius == 0) {
			    	    throw new ArithmeticException();
			    	}
					datar.circle();
					System.out.println("Luas dari Lingkaran adalah "+datar.getlarge());
					System.out.println("Keliling dari Lingkaran adalah "+datar.getperimeter());
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				
			}while (true);
			case 7:
			 
			do{
				try{
					System.out.println("Masukkan panjang diagonal 1 Layang-layang : ");
					diagonal1 = input.nextFloat();
					datar.setdiagonal1(diagonal1);
					if (diagonal1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (diagonal1 == 0) {
			    	    throw new ArithmeticException();
			    	}
			    	
					System.out.println("Masukkan panjang diagonal 2 Layang-layang: ");
					diagonal2 = input.nextFloat();
					datar.setdiagonal2(diagonal2);
					if (diagonal2 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (diagonal2 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan sisi panjang : ");
					side1 = input.nextFloat();
					datar.setside1(side1);
					
					if (side1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side1 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan sisi pendek : ");
					side2 = input.nextFloat();
					datar.setside2(side2);
					if (side2 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side2 == 0) {
			    	    throw new ArithmeticException();
			    	}
					datar.kyte();
					System.out.println("Luas dari Layang-layang adalah "+datar.getlarge());
					System.out.println("Keliling dari Layang-layang adalah "+datar.getperimeter());
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				
			}while (true);	
			case 8 :
				
				do{
					try{
						System.out.println("Masukkan alas jajargenjang : ");
						base = input.nextFloat();
						datar.setbase(base);
						if (base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (base == 0) {
				    	    throw new ArithmeticException();
				    	}
						
						System.out.println("Masukkan tinggi jajargenjang : ");
						height = input.nextFloat();
						datar.setheight(height);
						if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
						
						System.out.println("Masukkan sisi miring jajargenjang : ");
						side = input.nextFloat();
						datar.setside(side);
						if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}
						datar.parallelogram();
						System.out.println("Luas dari jajargenjang adalah "+datar.getlarge());
						System.out.println("Keliling dari jajargenjang adalah "+datar.getperimeter());
						
					}
				    catch(InputMismatchException e){
				    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("==============================");
						    if ( choice.contentEquals("YA") )break;
						    else if( choice.contentEquals("TIDAK") )break;
						    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
				    
				    if ( choice.contentEquals("YA") )continue;
				    else if( choice.contentEquals("TIDAK") )break;
					
				}while (true);
			case 9 :
				do{
					try{
						System.out.println("Masukkan panjang sisi segitiga : ");
						side = input.nextFloat();
						datar.setside(side);
						if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}
						
						System.out.println("Masukkan tinggi segitiga : ");
						height = input.nextFloat();
						datar.setheight(height);
						if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
						datar.EquilateralTriangle();
						
						System.out.println("Luas dari segitiga sama sisi adalah "+datar.getlarge());
						System.out.println("Keliling dari segitiga sama sisi adalah "+datar.getperimeter());
					
					}
				    catch(InputMismatchException e){
				    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("==============================");
						    if ( choice.contentEquals("YA") )break;
						    else if( choice.contentEquals("TIDAK") )break;
						    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
				    
				    if ( choice.contentEquals("YA") )continue;
				    else if( choice.contentEquals("TIDAK") )break;
					
				}while (true);
			default:
			System.out.println("Tidak sesuai dengan pilihan yang ada :)");
			do{ 
			    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("=================================");
			    
				    if (choice.contentEquals("YA")){
				    	break;
				    }
				    else if(choice.contentEquals("TIDAK")){
				    	break;
				    }
				    else {
				    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }
				    
	    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
			 break;
			}
				if (pilihan == 0)break;
			    if (choice.contentEquals("TIDAK"))break;
			    if (choice.contentEquals("YA"))continue;
			    break;
		case 2:
				
				System.out.println("\n");
				System.out.println("-----------------Bangun Ruang------------------");
				System.out.println("***********************************************"); 	
				System.out.println("1. Balok");
				System.out.println("2. Kubus");
				System.out.println("3. Tabung");
				System.out.println("4. Kerucut");
				System.out.println("5. Prisma Segitiga");
				System.out.println("6. Limas segiempat");
				System.out.println("7. Bola");
				System.out.println("0. Keluar");
				System.out.println("***********************************************"); 
				System.out.print("Silahkan masukkan nomor yang anda pilih : ");
				try{
					pilihan1 = input.nextInt();	    			
				}catch(InputMismatchException e){
					System.out.println("\nMaaf inputan yang anda masukkan bukan bilangan bulat");
					System.out.print("DAN ");    		
				input.nextLine();	
				do{ 
				    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("=================================");
				    
					    if (choice.contentEquals("YA")){
					    	break;
					    }
					    else if(choice.contentEquals("TIDAK")){
					    	break;
					    }
					    else {
					    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
					    }
					    
		    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
					break;
				}
					if (pilihan1 == 0)break;
				    if (choice.contentEquals("TIDAK"))break;
				    if (choice.contentEquals("YA"))continue;
			
		switch(pilihan1){
		case 0:
			break;
		case 1:
			
			do{
				try{
					System.out.println("Masukkan panjang balok : ");
					float length = input.nextFloat();
					datar.setlength(length);
					if (length < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	
			    	if (length == 0) {
			    	    throw new ArithmeticException();
			    	}	
					
					System.out.println("Masukkan lebar balok : ");
					float widht = input.nextFloat();
					datar.setwidht(widht);
					if (widht < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	
			    	if (widht == 0) {
			    	    throw new ArithmeticException();
			    	}	
					
					System.out.println("Masukkan tinggi balok : ");
					float height1 = input.nextFloat();
					datar.setheight(height1);
					if (height1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	
			    	if (height1 == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println();
			    	datar.beam();
			    	
					System.out.println("Volume dari Balok adalah "+datar.getVolume());
					System.out.println("Luas Permukaan Balok adalah "+datar.getsurfArea());
				
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				    
			
			}while (true);
			break;
		case 2:
			System.out.println("Masukkan sisi kubus : ");
			
			do{
				try{
					side = input.nextFloat();
					datar.setside(side);
			    	if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println();
			    	datar.cube();
			    	
					System.out.println("Volume dari Kubus adalah "+datar.getVolume());
					System.out.println("Luas Permukaan Kubus adalah "+datar.getsurfArea());
			    	
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				    
				
			}while (true);	
			break;
		case 3:
			do{
				try{
					System.out.println("Masukkan jari-jari Tabung : ");
					radius = input.nextInt();
					datar.setradius(radius);
					if (radius < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (radius == 0) {
			    	    throw new ArithmeticException();
			    	}	
				
					System.out.println("Masukkan tinggi Tabung : ");
					height = input.nextFloat();
					datar.setheight(height);
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}	
					System.out.println();
					datar.tube();
					
					System.out.println("Volume dari Tabung adalah "+datar.getVolume());
					System.out.println("Luas Permukaan Tabung adalah "+datar.getsurfArea());
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);
			break;
		case 4:
			
			do{
				try{
					System.out.println("Masukkan jari-jari kerucut : ");
					radius = input.nextInt();
					datar.setradius(radius);
					if (radius < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (radius == 0) {
			    	    throw new ArithmeticException();
			    	}
			    	
			    	System.out.println("Masukkan garis pelukis : ");
					side = input.nextFloat();
					datar.setside(side);
					if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}
			    	
			    	System.out.println("Masukkan tinggi kerucut : ");
					height = input.nextFloat();
					datar.setheight(height);
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}	
			    	System.out.println();
			    	datar.cone();
					System.out.println("Volume dari Kerucut adalah "+datar.getVolume());
					System.out.println("Luas Permukaan Kerucut adalah "+datar.getsurfArea());
				
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);
			break;
		case 5:
			do{
				try{
					System.out.println("Masukkan panjang sisi alas pertama : ");
					side1 = input.nextFloat();
					datar.setside1(side1);
					if (side1 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side1 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan panjang sisi alas kedua : ");
					side2 = input.nextFloat();
					datar.setside2(side2);
					if (side2 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side2 == 0) {
			    	    throw new ArithmeticException();
			    	}
					
					System.out.println("Masukkan panjang sisi alas ketiga : ");
					float side3 = input.nextFloat();
					datar.setside3(side3);
					if (side3 < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side3 == 0) {
			    	    throw new ArithmeticException();
			    	}
					System.out.println("Masukkan tinggi prisma : ");
					height = input.nextFloat();
					datar.setheight(height);
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}
					System.out.println();
					datar.triangularprism();
					 
					System.out.println("Volume dari Prisma Segitiga adalah "+datar.getlarge());
					System.out.println("Luas Permukaan Prisma Segitiga adalah "+datar.getsurfArea());
				
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);	
			break;
		case 6:
			
			do{
				try{
					System.out.println("Masukkan panjang sisi alas : ");
					side = input.nextFloat();
					datar.setside(side);
					if (side < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (side == 0) {
			    	    throw new ArithmeticException();
			    	}
			    	
			    	System.out.println("Masukkan tinggi limas : ");
					height = input.nextFloat();
					datar.setheight(height);
					if (height < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (height == 0) {
			    	    throw new ArithmeticException();
			    	}
					System.out.println();
					datar.rectangularpyramid();
					
					System.out.println("Volume dari Limas Segiempat adalah "+datar.getVolume());
					System.out.println("Luas Permukaan Prisma Segitiga adalah "+datar.getsurfArea());
					
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
			    
			}while (true);
			break;
		case 7:
			
			do{
				try{
					System.out.println("Masukkan jari-jari Bola : ");
					radius = (int) input.nextFloat();
					datar.setradius(radius);
					if (radius < 0) {
			    	    throw new IllegalArgumentException();
			    	}
			    	if (radius == 0) {
			    	    throw new ArithmeticException();
			    	}
					System.out.println();
					datar.ball();
					 
					System.out.println("Volume dari Bola adalah "+datar.getVolume());
					System.out.println("Luas permukaan dari Bola adalah "+datar.getsurfArea());
				
				}
			    catch(InputMismatchException e){
			    	System.out.println("Maaf inputan yang anda masukkan bukan angka :) ");
			    }
			    catch(ArithmeticException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh nol :) ");
			    }
			    catch(IllegalArgumentException e){
			    	System.out.println("Maaf inputan yang anda masukkan tidak boleh negatif :) ");
			    }
			    input.nextLine();
			    do{ 
			    	System.out.println();
				    System.out.print("Silahkan coba lagi ? (YA/TIDAK)");
				    System.out.println();			   
				    choice=input.nextLine();
				    System.out.println("==============================");
					    if ( choice.contentEquals("YA") )break;
					    else if( choice.contentEquals("TIDAK") )break;
					    else System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
			    }while( (choice.contentEquals("YA") == false) || ( choice.contentEquals("TIDAK") ) == false);
			    
			    if ( choice.contentEquals("YA") )continue;
			    else if( choice.contentEquals("TIDAK") )break;
				
			}while (true);
			break;
		
		default:
			System.out.println("Tidak sesuai dengan pilihan yang ada :)");
			do{ 
			    System.out.print("\nIngin Coba lagi ? (YA/TIDAK)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("=================================");
			    
				    if (choice.contentEquals("YA")){
				    	break;
				    }
				    else if(choice.contentEquals("TIDAK")){
				    	break;
				    }
				    else {
				    	System.out.println("SILAHKAN INPUT 'YA' ATAU 'TIDAK'\n" );
				    }
				    
	    	}while( (choice.contentEquals("YA") == false ) || ( choice.contentEquals("TIDAK") == false) );
			 break;
		}
			    if (choice.contentEquals("TIDAK"))break;
			    if (choice.contentEquals("YA"))continue;
			    
				break;
	
			
		}while(true);
		}
	while(true);
	}
}
}